@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection



@section('h1')
  {{ $page_title }}
@endsection
@section('content')

  <section class="section-0 px-4 w-full mb-8">

    <div id="leftxhouse ">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
        <h1 itemprop="headline" class="headxhouse mb-5">{{ $page_title }}</h1>

        <div class="cl"></div>
        <div class="xhousecontent">
          <div itemprop="articleBody">
            <div class="xhousecontent">
              @include('page/' . strtolower($page_title) )
            </div>
            <div class="cl"></div>
          </div>
      </article>
    </div>

    <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
        <div class="ads_sidebar">
          <center>@yield('ads')</center>
        </div>
      </div>
    </aside>
  </section>


@endsection
