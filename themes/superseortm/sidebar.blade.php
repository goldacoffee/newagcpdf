<div class='widget PopularPosts ' data-version='1' id='PopularPosts1'>
  <h2>Random Post</h2>
  <div class='widget-content cloud-label-widget-content'>
    <ul>
      @foreach ($random_terms as $key => $term)
        <li>
          <a href='{{ permalink($term) }}'>{{ ucwords($term) }}</a>
        </li>


      @endforeach



    </ul>
  </div>
</div>
