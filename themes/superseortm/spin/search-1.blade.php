@php
$cleanTitle = agcPdfCleanKw($query);

@endphp

<p>
  {{ spin('{Are you|Do you}') }} {{ spin('{looking for|finding}') }} something related to
  <strong> {{ $cleanTitle }}</strong> ? {{ spin('{Well|Congrats}') }} you come to the right places.

  <br>
  <br>
  In this post i will share you about &nbsp;<strong>{{ $cleanTitle }}</strong> , not only the
  {{ spin('{Images|Pictures}') }} but also the PDF file related to <strong>{{ $cleanTitle }}</strong>

  <br>
  <br>

  @if (!empty($pdfs))
    We already found about &nbsp;<strong>{{ count($pdfs) }}</strong> related document for &nbsp;
    <strong>{{ $cleanTitle }}</strong>. And we also have about &nbsp;
    <strong>{{ count($results) }}</strong>&nbsp;{{ spin('{Images|Pictures}') }} about <strong>{{ $cleanTitle }}</strong>
  @endif
</p>
