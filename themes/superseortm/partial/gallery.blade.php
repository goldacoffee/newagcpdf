@php
shuffle($results);
@endphp

@foreach ($results as $key => $item)


  @if ($key < 9)
    <div class="gallery-item">
      <a href="{{ permalink($item['title']) }}" title="{{ $item['title'] }}">
        <img class="gallery-image" src="{{ $item['small'] }}" alt="{{ $item['title'] }}">
      </a>
    </div>
  @endif


@endforeach
