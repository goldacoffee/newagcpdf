<style>
  body {
    font: normal normal 16px "Source Sans Pro", Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    color: #404040;
    background: #222;
    padding: 0;
    line-height: 1.35
  }

  html body .region-inner {
    min-width: 0;
    max-width: 100%;
    width: auto
  }

  h2 {
    font-size: 22px
  }

  a:link,
  a:visited {
    text-decoration: none;
    color: #404040
  }

  a:hover {
    text-decoration: underline;
    color: #c00
  }

  .body-fauxcolumn-outer .fauxcolumn-inner {
    background: transparent;
    _background-image: none
  }

  .body-fauxcolumn-outer .cap-top {
    position: absolute;
    z-index: 1;
    height: 400px;
    width: 100%
  }

  .body-fauxcolumn-outer .cap-top .cap-left {
    width: 100%;
    background: transparent;
    _background-image: none
  }

  .content-inner {
    /* padding: 10px; */
    background-color: #fff
  }

  .header {
    float: left;
    width: 100%;
    background: #fff;
    height: auto;
    max-height: 60px;
    position: relative;
    z-index: 999;
    transition: 0.3s;
  }

  .container {
    max-width: 970px;
    margin: 0 auto;
    height: 60px
  }

  .logo {
    float: left;
    margin: 10px 0;
    transition: 0.3s;
  }

  .logo .description {
    display: none
  }

  .logo a,
  .logo p {
    float: left;
    transition: 0.3s;
    max-width: 250px;
  }

  .logo h1.title,
  .logo a,
  .logo p {
    font-size: 28px;
    margin: 0;
    padding: 0;
    color: #222;
    font-weight: 700;
    text-transform: uppercase;
  }

  .logo a img {
    max-width: 200px;
    vertical-align: middle;
    height: auto;
  }

  .toggle-mobile-btn {
    display: none;
    float: left;
    line-height: 60px;
    width: 28px;
    transition: 0.3s;
  }

  .toggle-mobile-btn span:after,
  .toggle-mobile-btn span:before {
    content: "";
    position: absolute;
    left: 0;
    top: -7px;
  }

  .toggle-mobile-btn span:after {
    top: 7px;
  }

  .toggle-mobile-btn span {
    position: relative;
    display: inline-block;
  }

  .toggle-mobile-btn span,
  .toggle-mobile-btn span:after,
  .toggle-mobile-btn span:before {
    width: 100%;
    height: 4px;
    background-color: #888;
    transition: all 0.3s;
    backface-visibility: hidden;
    border-radius: 2px;
  }

  .toggle-mobile-btn.open span {
    background-color: transparent;
  }

  .toggle-mobile-btn.open span:before {
    transform: rotate(45deg) translate(5px, 5px);
  }

  .toggle-mobile-btn.open span:after {
    transform: rotate(-45deg) translate(7px, -8px);
  }

  .navigation {
    float: left;
    position: relative;
    margin-left: 50px;
  }

  .search-box {
    float: right;
    position: relative;
  }

  .navigation ul {
    margin: 0;
    padding: 0
  }

  .navigation li {
    float: left;
    margin: 0 12px;
    position: relative;
    list-style: none;
  }

  .navigation li a {
    text-decoration: none;
    float: left;
    line-height: 60px;
    text-transform: uppercase;
    color: #4f4f4f;
    transition: 0.3s;
    font-size: 15px;
    font-weight: 700
  }

  .navigation li.sub-menu>a {
    padding-right: 20px;
  }

  .navigation li.sub-menu>a:after {
    content: "";
    position: absolute;
    right: 0;
    top: 50%;
    border-top: 5px solid #4f4f4f;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    margin-top: -4px;
  }

  .navigation li.sub-menu>a:hover:after {
    border-top-color: #F68E56;
  }

  .navigation li.sub-menu ul {
    display: none;
    position: absolute;
    top: 100%;
    width: 160px;
    background: #f6f6f6;
    border: 1px solid #ededed;
    border-top: 0;
    z-index: 99999;
  }

  .navigation li.sub-menu ul ul {
    top: 0;
    left: 100%;
  }

  .navigation li.sub-menu>ul a:after {
    display: none;
  }

  .navigation li.sub-menu:hover>ul {
    display: block;
  }

  .navigation li.sub-menu li {
    margin: 0;
    width: 100%;
  }

  .navigation li.sub-menu li a {
    padding: 8px 15px;
    width: 100%;
    box-sizing: border-box;
    line-height: normal;
    text-transform: capitalize;
  }

  .navigation li a:hover {
    color: #c00;
  }

  .navigation li.active a {
    color: #222;
  }

  .navigation li.sub-menu li a:hover {
    background: #F68E56;
    color: #fff;
  }

  .search-box a {
    float: left;
    line-height: 60px;
    transition: 0.3s;
  }

  .search-icon {
    font-size: 20px;
    cursor: pointer
  }

  .search-icon img {
    vertical-align: middle;
  }

  .search-input {
    display: none;
    position: absolute;
    width: 230px;
    top: 100%;
    right: 0;
    background-color: #f6f6f6;
    padding: 15px;
    border: 1px solid #ededed;
    border-top: 0;
  }

  .search-input input {
    width: 90%;
    padding: 8px 10px;
    border: 1px solid #e9e9e9;
    letter-spacing: 1px;
  }

  .header.fixed-header {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 9999;
    height: auto;
    transition: 0.3s;
    box-shadow: 0 6px 8px 0 rgba(0, 0, 0, .08);
  }

  .fixed-header .toggle-mobile-btn {
    line-height: 60px;
    transition: 0.3s;
  }

  .fixed-header .logo {
    margin: 14px 0;
    transition: 0.3s;
  }

  .fixed-header .logo a {
    transition: 0.3s;
    width: 250px;
    text-decoration: none;
  }

  .fixed-header .navigation li a {
    line-height: 60px;
    transition: 0.3s;
  }

  .fixed-header .search-box a {
    line-height: 60px;
    transition: 0.3s;
  }

  @media only screen and (max-width: 800px) {

    .header,
    .container {
      padding: 0;
      margin: 0 auto;
      display: block;
      width: 100%;
    }

    .search-box a {
      padding-right: 15px;
    }

    .toggle-mobile-btn {
      display: block;
      padding-left: 15px;
    }

    .logo {
      float: none;
      display: inline-block;
      margin: 12px 0;
      height: 30px;
    }

    .header {
      text-align: center;
      position: relative;
    }

    .navigation {
      text-align: left;
      width: 200px;
      margin: 0;
      transition: 0.3s;
      position: absolute;
      left: -212px;
      top: 60px;
      background-color: #e4e4e4;
      border-right: 1px solid #cdcdcd;
      border-bottom: 1px solid #cdcdcd;
    }

    .fixed-header .navigation {
      top: 60px;
      transition: 0.3s;
    }

    .navigation li {
      margin: 0;
      width: 100%;
    }

    .navigation li a,
    .fixed-header .navigation li a {
      padding: 0 20px;
      width: 100%;
      line-height: 40px;
    }

    .navigation li.sub-menu>a:after {
      right: 10px;
      top: 22px;
    }

    .navigation li a:hover,
    .navigation li.active a {
      color: #222;
    }

    .navigation li.sub-menu ul {
      position: relative;
      width: 100%;
      border: 0;
    }

    .navigation li.sub-menu li a {
      padding: 6px 30px;
      font-size: 15px;
      font-weight: 600
    }
  }

  .catmenu {
    margin: 0 auto;
    background: #f5f5f5;
    width: 100%;
    max-width: 970px;
    height: 40px;
    border-top: 1px solid #f9f9f9;
    margin-top: 5px
  }

  .catmenu ul {
    ist-style-type: none;
    margin: 0;
    padding: 0;
    position: absolute;
  }

  .catmenu li {
    display: block;
    float: left;
  }

  .catmenu li a {
    display: block;
    height: 40px;
    text-align: center;
    line-height: 40px;
    color: #333;
    font-weight: 700;
    font-size: 15px;
    padding: 0 14px;
  }

  .catmenu li:hover a {
    color: #fff;
    background: #c00
  }

  .catmenu li:hover ul a {
    color: #c00;
    height: 40px;
    line-height: 40px;
  }

  .catmenu li:hover ul a:hover {
    color: #c00;
  }

  .show-menu {
    color: #fff;
    background: #c00;
    text-align: center;
    padding: 10px 0;
    display: none;
    margin-bottom: 10px
  }

  .catmenu input[type=checkbox] {
    display: none;
    -webkit-appearance: none;
  }

  .catmenu input[type=checkbox]:checked~#menu {
    display: block;
  }

  @media screen and (max-width: 760px) {
    .catmenu {
      margin-bottom: 10px;
      margin-top: 1px
    }

    .catmenu ul {
      position: static;
      display: none;
    }

    .catmenu li {
      padding: 0;
      margin: 0;
      margin-bottom: 1px;
    }

    .catmenu ul li,
    .catmenu li a {
      width: 100%;
      background: #eee
    }

    .show-menu {
      display: block;
      margin-bottom: 0
    }
  }

  .tabs-outer {
    max-width: 970px;
    margin: 15px 0 0;
    padding: 0;
  }

  .tabs-inner .section:first-child {
    border-top: 1px solid #fff
  }

  .tabs-inner .section:first-child ul {
    margin-top: -1px;
    border-top: 1px solid #fff;
    border-left: 0 solid #fff;
    border-right: 0 solid #fff
  }

  .tabs-inner .widget ul {
    background: #f6f6f6 url(https://resources.blogblog.com/blogblog/data/1kt/simple/gradients_light.png) repeat-x scroll 0 -800px;
    _background-image: none;
    border-bottom: 1px solid #fff;
    margin-top: 0;
    margin-left: -30px;
    margin-right: -30px
  }

  .tabs-inner .widget li a {
    display: inline-block;
    padding: .6em 1em;
    font: normal normal 14px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    color: #9c9c9c;
    border-left: 1px solid #fff;
    border-right: 1px solid #fff
  }

  .tabs-inner .widget li:first-child a {
    border-left: none
  }

  .tabs-inner .widget li.selected a,
  .tabs-inner .widget li a:hover {
    color: #000;
    background-color: #efefef;
    text-decoration: none
  }

  .main-outer {
    border-top: 0 solid #fff
  }

  .fauxcolumn-left-outer .fauxcolumn-inner {
    border-right: 1px solid #fff
  }

  .fauxcolumn-right-outer .fauxcolumn-inner {
    border-left: 1px solid #fff
  }

  div.widget>h2,
  div.widget h2.title,
  div.widget>h4,
  div.widget h4.title {
    margin: 0 0 1em;
    font: normal bold 16px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    color: #000;
    text-transform: uppercase;
    border-bottom: 1px solid #eee;
    padding-bottom: 10px;
  }

  h1,
  h2,
  h3,
  h4 {
    font-weight: 700
  }

  .post-body h1,
  h1.post-title {
    margin: 0;
    font-size: 24px;
    line-height: normal;
  }

  .post-body h2 {
    margin: 15px 0;
    font-size: 22px
  }

  .post-body h3 {
    margin: 15px 0;
    font-size: 20px
  }

  .widget .zippy {
    color: #9c9c9c;
    text-shadow: 2px 2px 1px rgba(0, 0, 0, .1)
  }

  .widget .popular-posts ul {
    list-style: none;
    padding: 0
  }

  h2.date-header {
    font: normal bold 11px Arial, Tahoma, Helvetica, FreeSans, sans-serif
  }

  .date-header span {
    background-color: rgba(60, 60, 60, 0);
    color: #404040;
    padding: inherit;
    letter-spacing: inherit;
    margin: inherit
  }

  .main-inner {
    padding-top: 15px;
    padding-bottom: 10px
  }

  .main-inner .column-center-inner {
    padding: 0 15px
  }

  .main-inner .column-center-inner .section {
    /* margin: 0 15px */
  }

  .post {
    margin: 0 0 25px
  }

  h3.post-title,
  .comments h4 {
    font: normal bold 16px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    margin: .75em 0 0
  }

  .post-body {
    font-size: 100%;
    line-height: 1.4;
    position: relative;
    overflow: hidden;
  }

  .post-body img,
  .post-body .tr-caption-container {
    padding: 0;
    max-width: 100%;
    /* height: auto; */
    height: 100%;
  }

  .separator a {
    margin-left: 0 !important
  }

  .post-body .tr-caption-container {
    color: #404040
  }

  .post-body .tr-caption-container img {
    padding: 0;
    background: transparent;
    border: none;
    -moz-box-shadow: 0 0 0 rgba(0, 0, 0, .1);
    -webkit-box-shadow: 0 0 0 rgba(0, 0, 0, .1);
    box-shadow: 0 0 0 rgba(0, 0, 0, .1)
  }

  .post-header {
    line-height: 1.6;
    font-size: 90%;
    margin: 15px 0;
    padding: 0 0 10px;
    border-bottom: 1px solid #eee;
    color: #999;
    font-weight: 400;
  }

  .post-footer {
    margin: 20px -2px 0;
    padding: 5px 10px;
    color: #6c6c6c;
    background-color: #fafafa;
    border-bottom: 1px solid #efefef;
    line-height: 1.6;
    font-size: 90%
  }

  blockquote {
    margin: 5px 0;
    padding: 15px;
    background: #eee;
    border-left: 15px solid #ddd;
  }

  .comments h4 {
    font: normal bold 15px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    margin: .75em 0 0
  }

  #comments .comment-author {
    padding-left: 25px
  }

  .comment-body {
    margin: .5em 25px
  }

  .comment-footer {
    margin: .5em 25px 1.5em
  }

  .comment-body p {
    margin: 0
  }

  #comments .avatar-comment-indent .comment-author {
    margin-left: -45px;
    padding-left: 45px
  }

  .deleted-comment {
    font-style: italic;
    opacity: .5
  }

  #comment-actions {
    background: transparent;
    border: 0;
    padding: 0;
    position: absolute;
    height: 25px
  }

  #comments .blogger-comment-icon,
  .blogger-comment-icon {
    line-height: 16px;
    background: url(/img/b16-rounded.gif) left no-repeat;
    padding-left: 20px
  }

  #comments .openid-comment-icon,
  .openid-comment-icon {
    line-height: 16px;
    background: url(/img/openid16-rounded.gif) left no-repeat;
    padding-left: 20px
  }

  #comments .anon-comment-icon,
  .anon-comment-icon {
    line-height: 16px;
    background: url(/img/anon16-rounded.gif) left no-repeat;
    padding-left: 20px
  }

  .comment-form {
    clear: both;
    _width: 410px
  }

  .comment-link {
    white-space: nowrap
  }

  .paging-control-container {
    float: right;
    margin: 0 6px 0 0;
    font-size: 80%
  }

  .unneeded-paging-control {
    visibility: hidden
  }

  #comments-block .avatar-image-container img {
    -ms-interpolation-mode: bicubic;
    border: 1px solid #ccc;
    float: right
  }

  #comments-block .avatar-image-container.avatar-stock img {
    border-width: 0;
    padding: 1px
  }

  #comments-block .avatar-image-container {
    height: 37px;
    left: -45px;
    position: absolute;
    width: 37px
  }

  #comments-block.avatar-comment-indent {
    margin-left: 45px;
    position: relative
  }

  #comments-block.avatar-comment-indent dd {
    margin-left: 0
  }

  iframe.avatar-hovercard-iframe {
    border: 0 none;
    padding: 0;
    width: 25em;
    height: 9.4em;
    margin: .5em
  }

  .comments {
    clear: both;
    margin-top: 10px;
    margin-bottom: 0
  }

  .comments .comments-content {
    margin-bottom: 16px
  }

  .comments .comment .comment-actions a {
    padding: 5px 8px;
    background: #5f4d53;
    color: #fff;
    font-size: 12px;
    margin-right: 10px;
    border-radius: 5px;
  }

  .comments .comment .comment-actions a:hover {
    text-decoration: underline
  }

  .comments .comments-content .comment-thread ol {
    list-style-type: none;
    padding: 0;
    text-align: left
  }

  .comments .comments-content .inline-thread {
    padding: .5em 1em
  }

  .comments .comments-content .comment-thread {
    margin: 8px 0
  }

  .comments .comments-content .comment-thread:empty {
    display: none
  }

  .comments .comments-content .comment-replies {
    margin-left: 36px;
    margin-top: 1em
  }

  .comments .comments-content .comment {
    margin-bottom: 16px;
    padding-bottom: 8px
  }

  .comments .comments-content .comment:first-child {
    padding-top: 16px
  }

  .comments .comments-content .comment:last-child {
    border-bottom: 0;
    padding-bottom: 0
  }

  .comments .comments-content .comment-body {
    position: relative
  }

  .comments .comments-content .user {
    font-style: normal;
    font-weight: bold
  }

  .comments .comments-content .icon.blog-author {
    display: inline-block;
    height: 18px;
    margin: 0 0 -4px 6px;
    width: 18px
  }

  .comments .comments-content .datetime {
    margin-left: 6px;
    display: none
  }

  .comments .comments-content .comment-header,
  .comments .comments-content .comment-content {
    margin: 0 0 8px
  }

  .comments .comments-content .comment-content {
    text-align: left;
    text-align: left;
    font-size: 95%;
    font-family: Arial;
  }

  .comments .comments-content .owner-actions {
    position: absolute;
    right: 0;
    top: 0
  }

  .comments .comments-replybox {
    border: none;
    height: 250px;
    width: 100%
  }

  .comments .comment-replybox-single {
    margin-left: 48px;
    margin-top: 5px
  }

  .comments .comment-replybox-thread {
    margin-top: 5px
  }

  .comments .comments-content .loadmore a {
    display: block;
    padding: 10px 16px;
    text-align: center
  }

  .comments .thread-toggle {
    cursor: pointer;
    display: inline-block
  }

  .comments .continue {
    cursor: pointer
  }

  .comments .continue a {
    display: block;
    font-weight: bold;
    padding: .5em
  }

  .comments .comments-content .loadmore {
    cursor: pointer;
    margin-top: 3em;
    max-height: 3em
  }

  .comments .comments-content .loadmore.loaded {
    max-height: 0;
    opacity: 0;
    overflow: hidden
  }

  .comments .thread-chrome.thread-collapsed {
    display: none
  }

  .comments .thread-toggle {
    display: inline-block
  }

  .comments .thread-toggle .thread-arrow {
    display: inline-block;
    height: 6px;
    margin: .3em;
    overflow: visible;
    padding-right: 4px;
    width: 7px
  }

  .comments .thread-expanded .thread-arrow {
    background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAG0lEQVR42mNgwAfKy8v/48I4FeA0AacVDFQBAP9wJkE/KhUMAAAAAElFTkSuQmCC") no-repeat scroll 0 0 transparent
  }

  .comments .thread-collapsed .thread-arrow {
    background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAJUlEQVR42mNgAILy8vL/DLgASBKnApgkVgXIkhgKiNKJ005s4gDLbCZBiSxfygAAAABJRU5ErkJggg==") no-repeat scroll 0 0 transparent
  }

  .comments .avatar-image-container {
    float: left;
    max-height: 36px;
    overflow: hidden;
    width: 36px
  }

  .comments .avatar-image-container img {
    max-width: 36px
  }

  .comments .comment-block {
    margin-left: 48px;
    position: relative
  }

  .comments .hidden {
    display: none
  }

  @media screen and (max-device-width:480px) {
    .comments .comments-content .comment-replies {
      margin-left: 0
    }
  }

  table.tr-caption-container {
    padding: 4px;
    margin-bottom: .5em
  }

  td.tr-caption {
    font-size: 80%
  }

  .icon-action {
    border-style: none !important;
    margin: 0 0 0 .5em !important;
    vertical-align: middle
  }

  .comment-action-icon {
    width: 13px;
    height: 13px;
    margin-top: 3px
  }

  .delete-comment-icon {
    background: url("/img/icon_delete13.gif") no-repeat left;
    padding: 7px
  }

  #comment-popup {
    position: absolute;
    visibility: hidden;
    width: 100px;
    height: 20px
  }

  .comments h4 {
    font: normal bold 15px Arial, Tahoma, Helvetica, FreeSans, sans-serif;
    margin: .75em 0 0
  }

  blockquote {
    background: #f9f9f9;
    padding: 15px;
    margin: 10px
  }

  #comments .comment-author {
    padding-top: 1.5em;
    border-top: 1px solid #ffffff;
    background-position: 0 1.5em
  }

  #comments .comment-author:first-child {
    padding-top: 0;
    border-top: none
  }

  .avatar-image-container {
    margin: .2em 0 0
  }

  #comments .avatar-image-container img {
    border: 1px solid #eeeeee
  }

  .comments .comment-thread.inline-thread {
    background-color: #f9f9f9
  }

  .comments .continue {
    border-top: 1px solid #ddd
  }

  .comment-form p {
    margin: 15px 0;
  }

  .comments .comments-content .icon.blog-author {}

  .comments .comments-content .datetime {
    display: none;
  }

  .item-control {
    display: inline-block;
  }

  .blog-pager {
    background: transparent none no-repeat scroll top center
  }

  .blog-pager-older-link,
  .home-link,
  .blog-pager-newer-link {
    background-color: #fff;
    padding: 5px
  }

  .footer-outer {
    border-top: 0 dashed #bbb
  }

  .footerna {
    text-align: center;
    border-top: 1px solid #eee;
    padding: 15px 0 0;
    width: auto;
    margin: 0 10px;
    font-size: 90%
  }

  body.mobile {
    background-size: auto
  }

  .mobile .body-fauxcolumn-outer {
    background: transparent none repeat scroll top left
  }

  .mobile .body-fauxcolumn-outer .cap-top {
    background-size: 100% auto
  }

  .mobile .content-outer {
    -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, .15);
    box-shadow: 0 0 3px rgba(0, 0, 0, .15)
  }

  .mobile .tabs-inner .widget ul {
    margin-left: 0;
    margin-right: 0
  }

  .mobile .post {
    margin: 0
  }

  .mobile .main-inner .column-center-inner .section {
    margin: 0
  }

  .mobile .date-header span {
    padding: .1em 10px;
    margin: 0 -10px
  }

  .mobile h3.post-title {
    margin: 0
  }

  .mobile .blog-pager {
    background: transparent none no-repeat scroll top center
  }

  .mobile .footer-outer {
    border-top: none
  }

  .mobile .main-inner,
  .mobile .footer-inner {
    background-color: #fff
  }

  .mobile-index-contents {
    color: #404040
  }

  .mobile-link-button {
    background-color: #404040
  }

  .mobile-link-button a:link,
  .mobile-link-button a:visited {
    color: #fff
  }

  .mobile .tabs-inner .section:first-child {
    border-top: none
  }

  .mobile .tabs-inner .PageList .widget-content {
    background-color: #efefef;
    color: #000;
    border-top: 1px solid #fff;
    border-bottom: 1px solid #fff
  }

  .mobile .tabs-inner .PageList .widget-content .pagelist-arrow {
    border-left: 1px solid #fff
  }

  h2.date-header,
  .quickedit,
  .blog-feeds,
  #backlinks-container,
  .feed-links {
    display: none
  }

  .sidebar .widget li {
    list-style: none;
    border-bottom: 1px dashed #eee;
    padding-bottom: 10px;
    margin: 5px 0;
  }

  .sidebar .widget li:last-child {
    border-bottom: 1px dashed #fff;
  }

  .sidebar .widget {
    margin: 0 0 20px
  }

  .sidebar ul {
    list-style: none;
    padding: 0;
  }

  .cloud-label-widget-content {
    text-align: left;
    overflow: hidden
  }

  .cloud-label-widget-content .label-count {
    background: #446CB3;
    color: #fff !important;
    margin-left: -3px;
    white-space: nowrap;
    border-radius: 0;
    padding: 1px 4px !important;
    font-size: 12px !important;
    margin-right: 5px
  }

  .cloud-label-widget-content .label-size {
    background: #f5f5f5;
    display: block;
    float: left;
    font-size: 11px;
    margin: 0 5px 5px 0
  }

  .cloud-label-widget-content .label-size a,
  .cloud-label-widget-content .label-size span {
    height: 18px !important;
    color: #2f2f2f;
    display: inline-block;
    font-size: 11px;
    font-weight: 400 !important;
    text-transform: uppercase;
    padding: 6px 8px
  }

  .cloud-label-widget-content .label-size a {
    padding: 6px 10px
  }

  .cloud-label-widget-content .label-size a:hover {
    color: #fff !important
  }

  .cloud-label-widget-content .label-size,
  .cloud-label-widget-content .label-count {
    height: 30px !important;
    line-height: 19px !important
  }

  .cloud-label-widget-content .label-size:hover {
    background: #446CB3;
    color: #fff !important
  }

  .cloud-label-widget-content .label-size:hover a {
    color: #fff !important
  }

  .cloud-label-widget-content .label-size:hover span {
    background: #2f2f2f;
    color: #fff !important;
    cursor: pointer
  }

  .cloud-label-widget-content .label-size-1,
  .label-size-2,
  .label-size-3,
  .label-size-4,
  .label-size-5 {
    font-size: 100%;
    opacity: 10
  }

  .label-size-1,
  .label-size-2 {
    opacity: 100
  }

  .FollowByEmail td {
    width: 100%;
    float: left;
    box-sizing: border-box
  }

  .FollowByEmail .follow-by-email-inner .follow-by-email-submit {
    margin-left: 0;
    width: 100%;
    border-radius: 0;
    height: 30px;
    font-size: 11px;
    color: #fff;
    background-color: #446CB3;
    font-family: inherit;
    text-transform: uppercase;
    font-weight: 400;
    letter-spacing: 1px
  }

  .FollowByEmail .follow-by-email-inner .follow-by-email-submit:hover {
    background-color: #3a3a3a
  }

  .FollowByEmail .follow-by-email-inner .follow-by-email-address {
    padding-left: 10px;
    height: 30px;
    border: 1px solid #FFF;
    margin-bottom: 5px;
    box-sizing: border-box;
    font-size: 12px;
    font-family: inherit
  }

  .FollowByEmail .follow-by-email-inner .follow-by-email-address:focus {
    border: 1px solid #FFF
  }

  .FollowByEmail .widget-content {
    background-color: #f5f5f5;
    margin-top: 20px;
    padding: 20px;
    max-width: 270px
  }

  .FollowByEmail .widget-content:before {
    content: "Enter your email address to subscribe to this blog and receive notifications of new posts by email.";
    font-size: 13px;
    color: #222;
    line-height: 1.4em;
    margin-bottom: 5px;
    display: block;
    padding: 0 2px
  }

  .PopularPosts .item-thumbnail {
    margin: 0 15px 0 0 !important;
    width: 90px;
    height: 65px;
    float: left;
    overflow: hidden
  }

  .PopularPosts .item-thumbnail a {
    position: relative;
    display: block;
    overflow: hidden;
    line-height: 0
  }

  .PopularPosts ul li img {
    padding: 0;
    width: 90px;
    height: 65px
  }

  .PopularPosts .widget-content ul li {
    overflow: hidden;
    padding: 5px 0;
  }

  .sidebar .PopularPosts .widget-content ul li:first-child,
  .sidebar .custom-widget li:first-child {
    padding-top: 0;
    border-top: 0
  }

  .sidebar .PopularPosts .widget-content ul li:last-child,
  .sidebar .custom-widget li:last-child {
    padding-bottom: 0
  }

  .PopularPosts ul li a {
    font-size: 14px;
    line-height: 1.4em;
    margin: 2px 0;
    -ms-word-wrap: break-word;
    word-wrap: break-word;
    font-weight: 600;
    color: #333;
  }

  .PopularPosts ul li a:hover {
    color: #446CB3;
    text-decoration: none;
  }

  .PopularPosts .item-title {
    margin: 0;
    padding: 0;
    line-height: 0
  }

  .item-snippet {
    display: none;
    font-size: 0;
    padding-top: 0
  }

  .breadcrumbs {
    line-height: 1.2em;
    width: auto;
    overflow: hidden;
    padding: 0 0 10px;
    margin: 0 auto 10px;
    font-size: 87%;
    color: #888;
    font-weight: 300
  }

  .breadcrumbs a {
    display: inline-block;
    text-decoration: none;
    transition: all .3s ease-in-out;
    color: #777;
    font-weight: 400
  }

  .breadcrumbs a:hover {
    color: blue
  }

  #back-to-top {
    background: #E73037;
    color: #ffffff;
    padding: 8px 10px;
    font-size: 24px
  }

  .back-to-top {
    position: fixed !important;
    position: absolute;
    bottom: 20px;
    right: 20px;
    z-index: 999
  }

  .footer-inner {
    padding: 15px;
  }

  .social-profiles-widget {
    width: 100%;
    height: auto;
    padding: 0;
    float: right;
    margin: 10px 0
  }

  .social-profiles-widget ul {
    list-style: none;
    float: RIGHT;
    margin: 0;
    padding: 0
  }

  .social-profiles-widget ul li {
    list-style: none;
    margin: 0;
    display: inline;
    border-bottom: none;
  }

  .social-profile .fa {
    width: 50px;
    height: 45px;
    color: rgba(255, 255, 255, 0.8);
    text-align: center;
    line-height: 45px;
    font-size: 1.5em;
    margin: 0 7px 0 0;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
  }

  .social-profile.circle .fa {
    border-radius: 5%
  }

  .social-profile .fa-facebook {
    background: #3b5998
  }

  .social-profile .fa-twitter {
    background: #55acee
  }

  .social-profile .fa-youtube {
    background: #cc181e
  }

  .social-profile .fa-instagram {
    background: linear-gradient(15deg, #ffb13d, #dd277b, #4d5ed4);
  }

  .social-profile .fa-linkedin {
    background: #3371b7
  }

  .social-profile .fa:hover,
  .social-profile .fa:active {
    color: #FFF;
    -webkit-box-shadow: 1px 1px 3px #333;
    -moz-box-shadow: 1px 1px 3px #333;
    box-shadow: 1px 1px 3px #333
  }

  .status-msg-wrap {
    font-size: 100%;
    width: 100%;
    margin: 0 0 20px;
    position: relative;
  }

  .status-msg-border {
    border: none
  }

  #BlogSearch1 div.widget>h4,
  #BlogSearch1 div.widget h4.title,
  #BlogSearch1 h4 {
    display: none
  }

  #BlogSearch1_form {
    margin-top: 7px
  }

  input.gsc-input {
    padding: 5px 0;
    border: 1px solid #ddd;
    width: 100%
  }

  input.gsc-search-button {
    margin-left: 2px;
    padding: 4px 8px
  }

  #footer-wrapper {
    background: #fff;
    text-align: center;
    padding: 20px 0;
    margin: 0 auto;
    width: 100%;
    border-top: 1px solid rgba(0, 0, 0, 0.05);
    /* max-width: 970px */
  }

  #footer-wrapper .widget {
    margin: 15px 0;
  }

  .footer-column {
    position: relative;
    margin: 0 auto;
    clear: both;
    font-size: 14px;
    line-height: 24px;
    overflow: hidden;
    text-align: left;
    max-width: 990px
  }

  .footer-column h2,
  .footer-column h3,
  .footer-column h4 {
    font-size: 16px;
    line-height: 1.2em;
    display: block;
    font-weight: 700;
    padding: 0 0 10px 0;
    position: relative;
    color: #666
  }

  .footer-column h2:after,
  .footer-column h3:after,
  .footer-column h4:after {
    content: '';
    position: absolute;
    bottom: -1px;
    left: 0;
    width: 60px;
    height: 1px;
    background: #f03c29
  }

  .footer-menu {
    float: left;
    width: 31%;
    margin: 0 20px 20px 0
  }

  .footer-menu footer2,
  .footer-menu ul {
    margin: 1em auto
  }

  .footer-menu ul li {
    list-style-type: square;
    margin: 0 0 0 15px
  }

  #creditx a {
    color: #fff
  }

  #creditx a:hover {
    color: #ff6
  }

  #subscribe-footers {
    overflow: hidden;
    margin: 0 0 20px 0;
    width: 100%
  }

  #subscribe-footers p {
    margin: 1em 0
  }

  #subscribe-footers .emailfooter {
    margin: auto;
    text-align: center
  }

  #subscribe-footers .emailfooter form {
    margin: 0;
    padding: 0;
    float: left
  }

  #subscribe-footers .emailfooter input {
    background: rgba(255, 255, 255, 1);
    padding: 9px 12px;
    color: #999;
    font-size: 14px;
    margin-bottom: 10px;
    border: 1px solid rgba(0, 0, 0, 0.05);
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s
  }

  #subscribe-footers .emailfooter input:focus {
    color: #000;
    outline: none;
    border-color: rgba(0, 0, 0, 0.1)
  }

  #subscribe-footers .emailfooter .submitfooter {
    background: #f03c29;
    color: #fff;
    margin: 0 0 0 5px;
    font-size: 14px;
    cursor: pointer;
    border: 0;
    border-radius: 3px;
    transition: all .3s
  }

  #subscribe-footers .emailfooter .submitfooter:active,
  #subscribe-footers .emailfooter .submitfooter:hover {
    background: #cf3525;
    color: #fff
  }

  @media screen and (max-width:840px) {
    #footer-wrapper {
      padding: 20px 15px;
      border: 0;
      margin: 0 auto;
      width: auto
    }

    .footer-menu {
      float: left;
      width: 30%;
      margin: 0 20px 20px 0;
    }
  }

  @media only screen and (max-width:768px) {

    .page-menu ul li a:hover,
    .page-menu ul li:hover a {
      background: transparent
    }

    .footer-menu,
    #subscribe-footers {
      float: none;
      width: auto;
      margin: 0 10px 20px 10px
    }
  }

  @media screen and (max-width:640px) {
    #footer-wrapper {
      margin: auto;
      border-top: 1px solid rgba(0, 0, 0, 0.05);
      padding: 15px;
      width: auto;
    }
  }

  @media screen and (max-width:480px) {
    #subscribe-footers .emailfooter input {
      width: 100%
    }

    #subscribe-footers .emailfooter .submitfooter {
      margin: 0
    }

    #subscribe-footers .emailfooter form {
      margin: auto;
      float: none
    }

    .footer-menu,
    #subscribe-footers {
      float: none;
      width: auto;
      margin: 0 0 20px 0;
    }
  }

  .iklan-dalam {
    margin: 20px 0
  }

  .banner-atas {
    max-width: 970px;
    margin: 0 auto;
    max-height: 90px;
    padding-top: 10px
  }

  .secondWord {
    color: #4aa3df;
    transition: all .3s
  }

  .secondWord:hover,
  .header a:hover span {
    color: #c00;
  }

  .header-inner .Header .descriptionwrapper,
  #ContactForm1 {
    display: none
  }

  #Label1 {
    width: 100%;
    max-width: 320px
  }

  @media only screen and (min-width:260px) and (max-width:989px) {
    #Label1 {
      display: none !important;
    }
  }

  .techirshloader {
    padding: 15px;
    font-weight: 700;
    margin-bottom: 10px;
    text-align: center;
    background: #ffffff
  }

  .techirshloader a {
    text-transform: uppercase
  }

  .techirshloader a {
    text-decoration: none;
    width: 100%;
    color: #c00;
    border: solid 2px #c00;
    padding: 5px 15px;
    font-size: 14px;
    font-weight: 600;
  }

  .techirshloader a:hover {
    color: #fbfbfb;
    background: #c00
  }

  .status-msg-body {
    text-align: left;
    padding: 5px 0;
    z-index: 4;
    background: white;
    border-bottom: 1px dashed #eee;
  }

  body {
    min-width: 1050px
  }

  .content-outer,
  .content-fauxcolumn-outer,
  .region-inner {
    min-width: 1050px;
    max-width: 1050px;
    _width: 1050px
  }

  .main-inner .columns {
    padding-left: 0;
    padding-right: 360px
  }

  .main-inner .fauxcolumn-center-outer {
    left: 0;
    right: 360px;
    /* _width: expression(this.parentNode.offsetWidth - parseInt(0px) - parseInt(360px) + 'px') */
  }

  .main-inner .fauxcolumn-left-outer {
    width: 0
  }

  .main-inner .fauxcolumn-right-outer {
    width: 360px
  }

  .main-inner .column-left-outer {
    width: 0;
    right: 100%;
    margin-left: -0
  }

  .main-inner .column-right-outer {
    width: 360px;
    margin-right: -360px
  }

  #layout {
    min-width: 0
  }

  #layout .content-outer {
    min-width: 0;
    width: 800px
  }

  #layout .region-inner {
    min-width: 0;
    width: auto
  }

  body#layout div.add_widget {
    padding: 8px
  }

  body#layout div.add_widget a {
    margin-left: 32px
  }

  body#layout ul li {
    display: none;
  }

  body#layout #footer-container {
    margin: 0 0;
    padding: 0 20px
  }

  body#layout #footer-container {
    padding-bottom: 20px
  }

  body#layout #footer-wrapper {
    width: 100%
  }

  body#layout #footer-wrapper .footer-column {
    width: 100%;
    float: left;
  }

  body#layout .footer-menu {
    width: 27%;
    float: left
  }

  body#layout .banner-atas {
    padding: 90px 0 10px;
  }

  @media screen and (max-width:1024px) {

    body,
    .content-outer,
    .content-fauxcolumn-outer,
    .region-inner {
      width: 100% !important;
      min-width: 100% !important;
      padding: 0 !important
    }

    body .navbar {
      height: 0 !important;
    }

    .footer-inner {
      padding: 30px 0px !important;
    }
  }

  @media screen and (max-width: 800px) {
    .main-inner .columns {
      padding-left: 0;
      padding-right: 300px;
    }

    .main-inner .column-right-outer {
      width: 300px;
      margin-right: -300px;
    }

    .sidebar img {
      max-width: 100%;
      height: auto
    }

    .social-profiles-widget {
      width: auto;
      height: auto;
      padding: 0;
      float: none;
      margin: 0 auto;
    }

    .social-profiles-widget ul {
      list-style: none;
      float: none;
      margin: 0 auto;
      padding: 0;
      text-align: center;
    }

    .social-profile .fa {
      width: 38px;
      height: 38px;
      line-height: 38px;
      font-size: 1.3em;
    }

    .content-inner {
      padding: 0;
    }
  }

  @media screen and (max-width: 603px) {
    .main-inner .columns {
      padding-right: 0 !important;
    }

    .main-inner .column-right-outer {
      width: 100% !important;
      margin-right: 0 !important;
      margin-bottom: 25px;
    }
  }

  @media screen and (max-width:360px) {
    .social-profile .fa {
      width: 35px;
      height: 35px;
      line-height: 35px;
      font-size: 1.3em;
    }
  }

  .featuredpost h2,
  .featuredpost h4,
  .home-link {
    display: none
  }

  .post-summary h3 {
    margin: 10px 0;
    font-size: 22px;
    font-family: Oswald;
    font-weight: 500;
  }

  .post-summary h3 a:link,
  .post-summary h3 a:visited {
    color: #0b5fbb;
  }

  .post-summary {
    border-bottom: 1px solid #eee
  }

  .post-summary p {
    font-size: 95%;
    color: #888
  }

  #FeaturedPost1 .widget {
    margin: 0
  }

  .post-summary .image {
    height: 280px;
    object-fit: cover;
    border-radius: 5px;
  }

  @media screen and (max-width:800px) {
    .post-summary .image {
      height: auto;
    }
  }

  .post {
    margin: 0 0 20px 0;
    border-bottom: 1px solid #f1f1f1;
    padding-bottom: 20px;
  }

  .thumb {
    float: left;
    margin: 0 15px 10px 0;
    width: 170px;
    height: 90px
  }

  .thumb img {
    width: 100%;
    height: 100%;
    border-radius: 3px;
    object-fit: cover
  }

  h2.post-title,
  .post-body h2 {
    font-size: 18px;
    margin: 0 15px 8px 0
  }

  .post-body {
    font-size: 95%;
    line-height: 1.3;
    position: relative;
  }

  .post-footer {
    display: none
  }

  @media screen and (max-width:800px) {
    .thumb {
      float: none;
      margin: 0 0 15px 0;
      width: 100%;
      height: 100%
    }
  }




  .download-pdf {
    margin-bottom: 40px;
    text-align: center;
  }

  .another-document {
    margin-bottom: 30px;
    text-align: center;
    font-weight: bolder;

  }

  .pdf-table-area {
    width: 100%;
    margin-bottom: 30px;
  }

  .pdf-table-area thead tr {
    background-color: #009879;
    color: #ffffff;
    text-align: left;
  }

  .pdf-table-area th,
  .pdf-table-area td {
    padding: 12px 15px;
  }

  .pdf-table-area tbody tr {
    border-bottom: 1px solid #dddddd;
  }

  .pdf-table-area tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
  }

  .pdf-table-area tbody tr:last-of-type {
    border-bottom: 2px solid #009879;
  }

  .pdf-table-area tbody tr.active-row {
    font-weight: bold;
    color: #009879;
  }


  .gallery-container {
    max-width: 100rem;
    margin: 0 auto;
    /* padding: 0 2rem 2rem; */
  }


  .gallery {
    display: flex;
    flex-wrap: wrap;
    /* Compensate for excess margin on outer gallery flex items */
    margin: -1rem -1rem;
  }

  .gallery-item {
    /* Minimum width of 24rem and grow to fit available space */
    flex: 1 0 24rem;
    /* Margin value should be half of grid-gap value as margins on flex items don't collapse */
    margin: 1rem;
    box-shadow: 0.3rem 0.4rem 0.4rem rgba(0, 0, 0, 0.4);
    overflow: hidden;
  }

  .gallery-image {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
    transition: transform 400ms ease-out;
  }

  .gallery-image:hover {
    transform: scale(1.15);
  }

  /*

The following rule will only run if your browser supports CSS grid.

Remove or comment-out the code block below to see how the browser will fall-back to flexbox styling. 

*/

  @supports (display: grid) {
    .gallery {
      display: grid;
      grid-template-columns: repeat(auto-fit, minmax(10rem, 1fr));
      grid-gap: 2rem;
    }

    .gallery,
    .gallery-item {
      margin: 0;
    }
  }

</style>
