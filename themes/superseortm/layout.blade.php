<!DOCTYPE html>
<html class='v2' dir='ltr' lang='en' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b'
  xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>

<head>
  @includeIf('assets.bundle')
  <meta content='width=device-width, initial-scale=1' name='viewport' />
  <meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
  @include('header')
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js'></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,400i,700,700i">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700,700i">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  @includeIf('assets.style')

</head>



<body class='loading'>
  <div itemscope='itemscope' itemtype='http://schema.org/Webpage'>
    <meta content='{{ config('site.title') }}' itemprop='name' />
    <meta content='{{ config('site.description') }}' itemprop='description' />
  </div>
  <div class='content'>
    <div class='content-outer'>
      <div class='fauxborder-left content-fauxborder-left'>
        <div class='content-inner'>
          <header class='header'>
            <div class='container'>
              <div class='logo section' id='header'>
                <div class='widget Header' data-version='1' id='Header1'>
                  <div id='header-inner'>
                    <div class='titlewrapper'>
                      <h1 class='title'>
                        {{ config('site.title') }}
                      </h1>
                      <div class='descriptionwrapper'>
                        <p class='description'><span>{{ config('site.description') }}</span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='search-box'>
                <a action='/search' class='search-icon' method='get'><i class='fa fa-search'></i></a>
                <div class='search-input'>
                  <form action='/' method="POST">
                    <input name='q' placeholder='Search...' title='Type and hit enter' type='text' />
                  </form>
                </div>
              </div>
              <a class='toggle-mobile-btn' href='javascript: void(0);'>
                <span></span>
              </a>
              <nav class='navigation'>
                <ul>

                  @includeIf('partial.header-menu')

                </ul>
              </nav>
            </div>
          </header>
          <div class='clear'></div>

          {{-- <div class='catmenu'>
            <label class='show-menu' for='show-menu'>Show Menu</label>
            <input id='show-menu' role='button' type='checkbox' />
            <ul id='menu'>
              @foreach (range('A', 'Z') as $list)
                <li><a href='{{ permalink($list, 'list_term') }}'>{{ $list }}</a></li>
              @endforeach
            </ul>
          </div> --}}
          <div class='clear'></div>
          <!-- Banner Atas -->
          <div class='banner-atas'>
            <div class='banner-atas no-items section' id='Banner Atas' name='Banner Atas'></div>
          </div>
          <div class='clear'></div>
          <div class='main-outer'>
            <div class='fauxborder-left main-fauxborder-left'>
              <div class='region-inner main-inner'>
                <div class='columns fauxcolumns'>
                  <!-- corrects IE6 width calculation -->
                  <div class='columns-inner'>
                    <div class='column-center-outer'>
                      <div class='column-center-inner'>
                        <div class='featuredpost no-items section' id='Featured Post' name='Featured Post'></div>
                        <div class='main section' id='main' name='Main'>
                          <div class='main section' id='main' name='Main'>
                            <div class='widget Blog' data-version='1' id='Blog1'>
                              <div class='breadcrumbs' itemscope='itemscope'
                                itemtype='https://schema.org/BreadcrumbList'>
                                <span itemprop='itemListElement' itemscope='itemscope'
                                  itemtype='https://schema.org/ListItem'>
                                  <a href='/' itemprop='item' title='Home'>
                                    <meta content='1' itemprop='position' />
                                    <span itemprop='name'>Home</span>
                                  </a>
                                </span> &nbsp;&#8250;&nbsp; <span itemprop='itemListElement' itemscope='itemscope'
                                  itemtype='https://schema.org/ListItem'>
                                  <meta content='2' itemprop='position' />
                                  @yield('breadcumbs')
                                </span>
                              </div>
                              <div class='blog-posts hfeed'>
                                @yield('content')

                                <div class='halaman'></div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class='column-left-outer'>
                    <div class='column-left-inner'>
                      <aside>
                      </aside>
                    </div>
                  </div>
                  <div class='column-right-outer'>
                    <div class='column-right-inner'>
                      <aside>
                        <div class='sidebar section' id='sidebar-right-1'>
                          <div class='widget Feed' data-version='1' id='Feed1'>
                          </div>

                          @includeIf('sidebar')



                        </div>
                      </aside>
                    </div>
                  </div>
                </div>
                <div style='clear: both'></div>
                <!-- columns -->
              </div>
              <!-- main -->
            </div>
          </div>
        </div>
        <footer>
          <div id='footer-wrapper'>
            <div class='footer-column'>
              <div class='footer-menu section' id='footer-1'>
                <div class='widget HTML' data-version='1' id='HTML4'>
                  <h4 class='title'>About</h4>
                  <div class='widget-content'>
                    <div class='footer2'>
                    </div>
                  </div>
                </div>
              </div>
              <div class='footer-menu section' id='footer-2'>
                <div class='widget HTML' data-version='1' id='HTML5'>
                  <h4 class='title'>Web Pages</h4>
                  <div class='widget-content'>
                    <ul class='footer2 line'>
                      @includeIf('partial.footer-menu')
                    </ul>
                  </div>
                </div>
              </div>
              <div class='footer-menu section' id='footer-3'>

              </div>
            </div>
          </div>
          <div class='footer-outer'>
            <div class='fauxborder-left footer-fauxborder-left'>
              <div class='region-inner footer-inner'>
                <div class='footerna' id='footercredit'>
                  Copyright &#169;
                  <script type='text/javascript'>
                    var creditsyear = new Date();
                    document.write(creditsyear.getFullYear());
                  </script>
                  <a href='/'>{{ config('site.title') }}</a>. All rights reserved.
                </div>
              </div>
            </div>
          </div>
        </footer> <!-- content -->
      </div>
    </div>
  </div>
  </div>
  <script type='text/javascript'>
    window.setTimeout(function() {
      document.body.className = document.body.className.replace('loading', '');
    }, 10);
  </script>
  <script type='text/javascript'>
    function thumbnyabener(e, t) {
      for (var g = document.getElementById(e), m = g.getElementsByTagName("img"), r = 0; r < m.length; r++) m[r].src = m[
        r].src.replace(/\/s72\-c/, "/s" + t), m[r]
    }
    thumbnyabener("Blog1", 1600);
  </script>
  <div class='back-to-top'>
    <a href='#' id='back-to-top' title='back to top'>
      <i class='fa fa-chevron-up'></i>
    </a>
  </div>


  @includeIf('assets.js')
  @include('footer')

</body>

</html>
