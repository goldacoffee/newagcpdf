<?php

use Masterzain\Classes\Blade;
use Masterzain\Classes\FastCache;

class BaseController extends Application
{
    public function __construct() {
        $this->blade     = new Blade;
        $this->cache     = new FastCache;
		parent::__construct();
    }
}
