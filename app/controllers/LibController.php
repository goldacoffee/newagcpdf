<?php

use Masterzain\Classes\Sitemap as Map;
use Masterzain\Agc\Wallpaper;

class LibController extends BaseController
{

	public function __construct()
	{
		$this->map		      = new Map;
		$this->wallpaper    = new Wallpaper;
		parent::__construct();
	}

	function sitemap_index()
	{
		$data['sitemap']  	= range(1, config('options.library.sitemap_index'));
		echo $this->blade->header("text/xml", 200)->library("sitemap.index", $data);
	}

	function sitemap_main($loop)
	{
		$loop 				= str_replace(array('-', '.xml'), '', $loop);
		$data['sitemap']  	= $this->map->single_sitemap($loop, config('options.library.sitemap_main'));
		if (empty($data['sitemap'])) {
			$fakeLoop = rand(1, $this->map->count_sitemap());
			$data['sitemap']  = $this->map->single_sitemap($fakeLoop, config('options.library.sitemap_main'));
		}
		echo $this->blade->header("text/xml", 200)->library("sitemap.main", $data);
	}


	

	function sitemap_image()
	{
		$cache_key 			  = $this->wallpaper->random_key();
		$output 		  	  = $this->cache->get($cache_key);

		while (count($output['data']) < 1) {
			$output 		  	  = $this->cache->get($cache_key);
			sleep(3);
		}
		// get unique
		$tempArr = array_unique(array_column($output['data']['images'], 'url'));
		$output['data'] = array_intersect_key($output['data']['images'], $tempArr);

		$data['sitemap']  = $output['data'];





		/**
		 * added by ndower
		 * for preventing giving empty data
		 */

		// while (count($output['data']) < 1) {
		// 	$output 		  	  = $this->cache->get($cache_key);
		// 	$data['sitemap']  = $output['data'];
		// }


		echo $this->blade->header("text/xml", 200)->library("sitemap.image", $data);
	}

	function feed_image()
	{
		$cache_key 			  = $this->wallpaper->random_key();
		$output 		  	  = $this->cache->get($cache_key);


		// filter array
		$tempArr = array_unique(array_column($output['data']['images'], 'url'));
		$output['data']['images'] = array_intersect_key($output['data']['images'], $tempArr);




		$data['rss']      = $output['data']['images'];
		echo $this->blade->header("text/xml", 200)->library("feed.image", $data);
	}

	function feed_main()
	{
		$data['rss']      = random_terms(config('options.library.sitemap_main'));
		echo $this->blade->header("text/xml", 200)->library("feed.index", $data);
	}

	function robots()
	{
		$data['sitemap']  = range(1, config('options.library.sitemap_index'));
		echo $this->blade->header("text/plain", 200)->library("robots", $data);
	}

	function clear()
	{
		switch (Uri::segment(2)) {
			case "cache":
				$delete = delete_cache(config('cache.path.sub'));
				break;
			case "views":
				$delete = delete_cache(config('cache.path.views'));
				break;
			default:
				$views   = delete_cache(config('cache.path.views'));
				$delete  = delete_cache(config('cache.path.sub'));
		}

		if ($delete) {
			di('Deleting cache success');
		}
	}
}
