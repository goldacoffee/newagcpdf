<?xml version="1.0" encoding="UTF-8" ?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/"
  xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
  version="2.0">
  <channel>
    <atom:link href="{{ home_url() }}/feed" rel="self" type="application/rss+xml" />
    <title>{{ sitename() }} RSS</title>
    <description>{{ config('site.description') }} RSS</description>
    <link>{{ home_url() }}</link>
    <lastBuildDate>{{ date('r') }}</lastBuildDate>
    @if ($rss)
      @foreach ($rss as $item)
        <item>
          <title>{{ clean($item['title']) }}</title>
          <link>{{ permalink($item['title']) }}</link>
          <description>
            <![CDATA[<div><img width="150" height="150" src="{{ $item['url'] }}" alt="{{ clean($item['title']) }}" title="{{ clean($item['title']) }}"/></div>{{ clean($item['title']) }} ...]]>
          </description>
          <pubDate>{{ date('r') }}</pubDate>
          <guid>{{ permalink($item['title']) }}</guid>
        </item>
      @endforeach
    @endif
  </channel>
</rss>
